import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, QPushButton
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon

def get_icon_list(file_path):
    file = open(file_path, 'r')
    return file.read().split("\n")


class IconWindow(QMainWindow):
    def __init__(self,file_path):
        QMainWindow.__init__(self)

        self.setMinimumSize(QSize(640, 480))
        self.setWindowTitle("Icon from Theme")

        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)

        gridLayout = QGridLayout(self)
        centralWidget.setLayout(gridLayout)

        #get_icon_list=["weather-overcast","document-new","list-remove"]
        for i,icon in enumerate(get_icon_list(file_path)):
            print(i,icon)
            but=QPushButton(icon)
            ic=QIcon.fromTheme(icon)
            but.setIcon(ic)
            col=int(i/4)
            row=i-col*4
            gridLayout.addWidget(but,col,row)


if __name__ == "__main__":
    # https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
    file_path_list=[]
    file_path_list.append("db_icons.txt")
    file_path_list.append("StandardActionIcons.txt")
    file_path_list.append("StandardApplicationCategoryDeviceIcons.txt")
    file_path_list.append("StandardEmblemEmotionMIMEPlaceIcons.txt")

    n=int(input("Choose your file number (0 to 3) : "))
    if not(n>=0 and n<=3):
        n=1
    file_path=file_path_list[n] # from 0 to 3
        
        
    app = QtWidgets.QApplication(sys.argv)
    mainWin = IconWindow(file_path)
    mainWin.show()
    sys.exit( app.exec_() )
